# Generated by Django 2.0 on 2018-03-08 15:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Characteristics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('frame_type', models.CharField(default='intreaga', max_length=50, verbose_name='tip rama')),
                ('genre', models.CharField(default='femei', max_length=50, verbose_name='gen')),
                ('material', models.CharField(default='plastic', max_length=50, verbose_name='material rama')),
                ('shape', models.CharField(default='ochi de pisica', max_length=50, verbose_name='forma rama')),
                ('color', models.CharField(default='neagra', max_length=50, verbose_name='culoare rama')),
                ('shippping', models.CharField(default='1-3 zile lucratoare', max_length=50, verbose_name='termen livrare')),
            ],
        ),
        migrations.CreateModel(
            name='Dimensions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('height', models.IntegerField()),
                ('width', models.IntegerField()),
                ('nasal_bridge', models.IntegerField()),
                ('arm_length', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Glasses',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to='glasses-pics/')),
                ('characteristics', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='homepage.Characteristics')),
                ('dimensions', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='homepage.Dimensions')),
            ],
        ),
        migrations.CreateModel(
            name='GlassesType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('glasses_type', models.CharField(max_length=50, verbose_name='tip ochelari')),
            ],
        ),
        migrations.CreateModel(
            name='Lens',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('r_sphere', models.FloatField()),
                ('r_cylinder', models.FloatField()),
                ('r_axis', models.IntegerField()),
                ('l_sphere', models.FloatField()),
                ('l_cylinder', models.FloatField()),
                ('l_axis', models.IntegerField()),
                ('pupillary_distance', models.FloatField()),
            ],
        ),
        migrations.AddField(
            model_name='glasses',
            name='glasses_type',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='homepage.GlassesType'),
        ),
        migrations.AddField(
            model_name='glasses',
            name='lens',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='homepage.Lens'),
        ),
    ]
