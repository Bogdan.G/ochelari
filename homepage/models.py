from django.db import models


class Characteristics(models.Model):
    frame_type = models.CharField(
        "tip rama", max_length=50, default="intreaga")
    genre = models.CharField("gen", max_length=50, default="femei")
    material = models.CharField(
        "material rama", max_length=50, default="plastic")
    shape = models.CharField(
        "forma rama", max_length=50, default="ochi de pisica")
    color = models.CharField("culoare rama", max_length=50, default="neagra")
    shippping = models.CharField(
        "termen livrare", max_length=50, default="1-3 zile lucratoare")


class Dimensions(models.Model):
    height = models.IntegerField()
    width = models.IntegerField()
    nasal_bridge = models.IntegerField()
    arm_length = models.IntegerField()


class Lens(models.Model):
    r_sphere = models.FloatField()
    r_cylinder = models.FloatField()
    r_axis = models.IntegerField()
    l_sphere = models.FloatField()
    l_cylinder = models.FloatField()
    l_axis = models.IntegerField()
    pupillary_distance = models.FloatField()


class GlassesType(models.Model):
    glasses_type = models.CharField("tip ochelari", max_length=50)


class Glasses(models.Model):
    image = models.ImageField(
        upload_to='glasses-pics/', blank=True, null=True)
    glasses_type = models.OneToOneField(
        GlassesType, on_delete=models.PROTECT, blank=True, null=True)
    characteristics = models.OneToOneField(
        Characteristics, on_delete=models.PROTECT, blank=True, null=True)
    dimensions = models.OneToOneField(
        Dimensions, on_delete=models.PROTECT, blank=True, null=True)
    lens = models.OneToOneField(
        Lens, on_delete=models.PROTECT, blank=True, null=True)

    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
