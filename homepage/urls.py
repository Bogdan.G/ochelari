from django.urls import path
from django.utils.translation import gettext_lazy as _
from . import views


urlpatterns = [
    # path(_('search-results/'), views.search_results, name="search-results"),
    path('', views.homepage_view, name='homepage'),
]
