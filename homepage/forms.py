from django import forms

from .models import Glasses, Lens, Characteristics, Dimensions, GlassesType


class GlassesForm(forms.ModelForm):

    class Meta:
        model = Glasses
        fields = ['image', 'glasses_type',
                  'characteristics', 'dimensions', 'lens', ]
