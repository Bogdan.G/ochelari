from django.contrib import admin

from .models import Glasses, Lens, Characteristics, Dimensions, GlassesType 


admin.site.register(Glasses)
admin.site.register(Lens)
admin.site.register(Characteristics)
admin.site.register(Dimensions)
admin.site.register(GlassesType)
