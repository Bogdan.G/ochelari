from django.shortcuts import render
from django.http import HttpResponseRedirect

from homepage.models import Glasses
from homepage.forms import GlassesForm


def glasses_view(request):

    if request.method == "POST":
        form = GlassesForm(request.POST, request.FILES)

        if form.is_valid():
            print('worked')
            form.save()

            return HttpResponseRedirect('.')

    else:
        form = GlassesForm()
        glasses = Glasses.objects.all()

        return render(request, 'glasses/glasses.html',
                      {'form': form,
                       'glasses': glasses, })
